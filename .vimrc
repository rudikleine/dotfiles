set showmode
set autoindent tabstop=4
set expandtab
syntax on
set number
set relativenumber

" Govim setting
filetype plugin indent on


" Abbreviations for Vim
iabbrev _sh #!/bin/bash
iabbrev _gb 🇬🇧
iabbrev _de 🇩🇪
iabbrev _method func (r receiver) funcName() return {}

" Disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" CTRL+P
set runtimepath^=~/.vim/bundle/ctrlp.vim

colorscheme nord

set statusline+=%{wordcount().words}\ words
set laststatus=2    " enables the statusline.
